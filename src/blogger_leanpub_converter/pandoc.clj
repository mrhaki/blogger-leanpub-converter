(ns blogger-leanpub-converter.pandoc
  (:require [clojure.java.shell :as sh]
            [clojure.string :refer [join]])
  (:import [java.io File]))

(defn- create-temporary-file
  [prefix]
  (File/createTempFile prefix nil))

(defn- html->markup
  "Convert HTML to given format using pandoc."
  [html format]
  (let [input-file      (create-temporary-file "html")
        input-filename  (.getAbsolutePath input-file)
        output-file     (create-temporary-file "markdown")
        output-filename (.getAbsolutePath output-file)]
    (.deleteOnExit input-file)
    (.deleteOnExit output-file)
    (spit input-file html)
    (sh/sh "pandoc"
           "-f" "html"
           "-t" format
           "--markdown-headings=atx"
           "--wrap=none"
           "--no-highlight"
           "-o" output-filename
           input-filename)
    (slurp output-file)))

(defn html->markdown_leanpub
  "Convert HTML to Leanpub markdown."
  [html]
  (let [options ["+fenced_code_blocks"
                 "-backtick_code_blocks"
                 "-simple_tables"
                 "+pipe_tables"]]
    (html->markup html (str "markdown" (join options)))))

(comment

  (let [options ["+fenced_code_blocks"
                 "-backtick_code_blocks"
                 "-fenced_code_attributes"
                 "-simple_table"
                 "+pipe_tables"]]
    (str "markdown" (join options)))
  (str "markdown"  (join ["-table"]))

  (html->markdown_leanpub "<html><head></head><body><a name=\"top\"/><h2 id=\"https://mrhaki.com/a\">title</h2><p>When we make some small <a href=\"#https://mrhaki.com/a\">link</a> changes to <code>stylesheet.css</code> and run the <code>groovydoc</code> task we see the following example output:</p><p class=\"separator\" style=\"clear: both; text-align: center;\"><img src=\"https://4.bp.blogspot.com/-FVvq5yl-gfw/VqdVo7EHfRI/AAAAAAAALc4/mNdUCKrFsv4/s1600/groovydoc.png\" /></div><p style=\"clear:both;\">Notice we have our custom header, the Gravatar image and custom CSS.</p><p>Written with Groovy 2.4.5.</p><p><a href=\"\">Original post</a> written on March 5, 2021</p></body></html>")
  


  (def html "<p>In Groovy we can use the <code>@Delegate</code> annotation <a href=\"http://mrhaki.blogspot.com/2009/08/groovy-goodness-delegate-to-simplify.html\">for some time now</a> to automatically include methods from a delegated class into our own class. When the method is invoked on our class, it will be delegated to the correct class. Since Groovy 2.2 we can use the <code>includes</code> and <code>excludes</code> annotation parameters to define which methods of the delegated class we want to use in our own class. If any required method from the delegated class, which can also be defined in an interface, is left out when we use <code>includes</code> and <code>excludes</code>, then we get a compilation error, telling us to implement the required method from the delegated class or interface.</p><p>In the following sample we define a <code>SimpleEvent</code> class with a <code>start</code> and <code>end</code> property we delegate to the <code>Date</code> class. For the <code>start</code> property we don't want the <code>after()</code> method to be delegated, because we want to delegate it to the <code>Date</code> object of the <code>end</code> property. And for the <code>end</code> property we want to delegate the <code>before()</code> delegated method of the <code>Date</code> object of the <code>start</code> property.</p>
  <pre class=\"plain\">
@groovy.transform.ToString
class SimpleEvent {
    @Delegate(excludes=['after']) Date start
    @Delegate(excludes=['before']) Date end
    String description
}

def event = new SimpleEvent(description: 'Groovy seminar',
                            start: nextWeek, 
                            end: nextWeek + 3)
// Helper get methods:
def getToday() {
    new Date().clearTime()
}

def getNextWeek() {
    today + 7
}
  </pre><p>Code written with Groovy 2.2.</p>"))
