(ns blogger-leanpub-converter.markdown
  (:require [clojure.string :as str]
            [blogger-leanpub-converter.html :refer [make-url-id]]))

(defn fix-code-block
  [markup]
  (str/replace markup #"~~~ .*\n" (str "{lang=text}" \newline "~~~" \newline)))

(defn fix-section-title
  [markup url]
  (let [https-url (make-url-id url)]
    (str/replace markup #"(?<!#)##(?!#)\s+(.*)" (str "{#" https-url "}" \newline "## $1"))))


(comment

  (prn (fix-code-block blogger-post))
  (fix-section-title blogger-post "https://mrhaki.blogspot.com/2016/03/groovy-goodness-using-tuples.html")
  
  
  (fix-code-block "~~~ plain
$ mkdir spock-intro
$ cd spock-intro
$ mkdir -p src/main/groovy/com/mrhaki/blog src/test/groovy/com/mrhaki/blog
~~~
                   ")


  (def blogger-post "## Groovy Goodness: Tuples With Up To 9 Items\n\nA tuple is an ordered, immutable list of elements. Groovy [supported tuples with one or two elements](https://mrhaki.blogspot.com/2016/03/groovy-goodness-using-tuples.html) before Groovy 2.5.0.\n\n### Source \n\n~~~ {.groovy}\n// We can define the types of the elements when we \n// construct a tuple.\ndef tuple3 = new Tuple3<String, Integer, BigDecimal>('add', 2, 40.0)\n\n// We can use first, second, third properties\n// to get values from the tuple.\nassert tuple3.first == 'add'\nassert tuple3.second == 2\nassert tuple3.third == 40.0\n\n// We can use the [index] syntax to get element.\nassert tuple3[0] == 'add'  \n\n// Fully typed tuple.\nTuple4<String, Integer, BigDecimal, Integer> tuple4 = \n    new Tuple4<>('subtract', 100, 55.0, 3)\n    \nassert tuple4.first == 'subtract'\nassert tuple4.second == 100\nassert tuple4.third == 55.0\nassert tuple4.fourth == 3\nassert tuple4[-1] == 3\n\n// With subTuple we can get subsequent\n// values from the tuple as a new tuple.\nassert tuple4.subTuple(2, tuple4.size()) == new Tuple2<BigDecimal, Integer>(55.0, 3)\n\n// We can imagine how to work with Tuple4..Tuple8 :-)\n// ...\n\n// Finally a tuple with 9 items.\ndef tuple9 = new Tuple9('Groovy', 'rocks', 'and', 'is', 'fun', 'to', 'use', 'as', 'language')\n\nassert tuple9.fifth == 'fun'\nassert tuple9.sixth == 'to'\nassert tuple9.seventh == 'use'\nassert tuple9.eighth == 'as'\nassert tuple9.ninth == 'language'\n\n// Tuple extends AbstractList, so we can\n// use all methods from List as well.\nassert tuple9.join(' ') == 'Groovy rocks and is fun to use as language'\n~~~\n\nWritten with Groovy 2.5.0.\n\n[Original post](https://blog.mrhaki.com/2018/06/groovy-goodness-tuples-with-up-to-9.html) written on June 22, 2018\n")
  )
