(ns blogger-leanpub-converter.image
  (:require [clojure.java.shell :as sh]))

(defn- get-image-width-pixels
  [file]
  (let [file-name   (.getAbsolutePath file)
        sips-output (sh/sh "sips" "--getProperty" "pixelWidth" "--oneLine" file-name)]
    (when (= (:exit sips-output) 0)
      (Long/parseLong (last (re-seq #"\d+" (:out sips-output)))))))

(defn- get-image-height-pixels
  [file]
  (let [file-name   (.getAbsolutePath file)
        sips-output (sh/sh "sips" "--getProperty" "pixelHeight" "--oneLine" file-name)]
    (when (= (:exit sips-output) 0)
      (Long/parseLong (last (re-seq #"\d+" (:out sips-output)))))))

(defn convert-image!
  [file]
  (let [image-width  (get-image-width-pixels file)
        max-width    940
        image-height (get-image-height-pixels file)
        max-height   1454
        dpi          150
        file-name    (.getAbsolutePath file)]
    (when (< max-height image-height)
      (sh/sh "sips"
             "-s" "dpiHeight" (String/valueOf dpi)
             "-s" "dpiWidth" (String/valueOf dpi)
             "--resampleHeight" (String/valueOf max-height)
             file-name))
    (when (< max-width image-width)
      (sh/sh "sips"
             "-s" "dpiHeight" (String/valueOf dpi)
             "-s" "dpiWidth" (String/valueOf dpi)
             "--resampleWidth" (String/valueOf max-width)
             file-name))
    file))

(comment

  (require '[clojure.java.io :as io])

  (io/copy (io/file "test-resources" "image-width-source.png") (io/file "test-resources" "image-width-work.png"))

  (convert-image! (io/file "sample" "awesome-asciidoctor-notebook" "images" "title-sample-full.png"))
  (convert-image! (io/file "sample" "awesome-asciidoctor-notebook" "images" "adoc-callout-auto-2.png"))

  (get-image-height-pixels (io/file "sample" "awesome-asciidoctor-notebook" "images" "title-sample-full.png"))

  (get-image-width-pixels (io/file "sample" "awesome-asciidoctor-notebook" "images" "title-sample-full.png"))
  (get-image-width-pixels (io/file "sample" "spock-framework-notebook" "images" "spock-config-report1.png"))

  (sh/sh "sips" "--getProperty" "pixelHeight" "--oneLine" (.getAbsolutePath (io/file "sample" "spock-framework-notebook" "images" "title-sample-full.png")))

  (def result "/Users/mrhaki/Projects/clojure/blogger-leanpub-converter/sample/spock-framework-notebook/images/spock-run-order-2-1.png|  pixelWidth: 940|")
  (last (re-seq #"\d+" result))

  (str "Hi " (when (= "test1" "test") "Test") (when (= "test" "test") "test"))

  ;; empty line
  )
  
  

