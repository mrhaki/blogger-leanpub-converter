(ns blogger-leanpub-converter.html
  (:require [clojure.walk :refer [postwalk]]
            [clojure.string :as str]
            [clojure.zip :as zip]
            [hickory.core :as hickory]
            [hiccup.core :as h]
            [hiccup.element :refer [link-to]])
  (:import [java.time OffsetDateTime]
           [java.time.format DateTimeFormatter]))

(defn- key-value?
  [x]
  (and (vector? x)
       (= 2 (count x))
       (keyword? (first x))))

(defn- is-attr?
  [attr name]
  (and (key-value? attr)
       (or (and (set? name)
                (name (first attr)))
           (and (= name (first attr))))))

(defn- is-tag?
  [tag name]
  (and (vector? tag)
       (keyword? (first tag))
       (= name (first tag))))

(defn- has-child-tag?
  [loc tag]
  (-> loc zip/down zip/right zip/right zip/node (is-tag? tag)))

(defn- append-to-tag
  [html tag-name new-html]
  (let [append (fn [tag]
                 (if (is-tag? tag tag-name)
                   (conj tag new-html)
                   tag))]
    (postwalk append html)))

(defn- insert-at
  [coll i e]
  (let [[before after] (split-at i coll)]
    (vec (concat before [e] after))))

(defn- prepend-to-tag
  [html tag-name new-html]
  (let [prepend (fn [tag]
                  (if (is-tag? tag tag-name)
                    (insert-at tag 2 new-html)
                    tag))]
    (postwalk prepend html)))

(defn- make-url-https
  [url]
  (if (str/starts-with? url "http:")
    (str/replace url "http" "https")
    url))

(defn make-url-id
  "Turn a blogger post URL to a section id"
  [url]
  (str/replace url #"^(http|https)://blog.mrhaki.com/(\d+)/(\d+)/(.*).html$"
               (fn [[_ _ year month title]] (str "id-" year "-" month "-" title))))

(defn html->hiccup
  [html-string]
  (-> html-string
      hickory/parse
      hickory/as-hiccup))

(defn- extract-image-file-name
  [url]
  (let [last-slash (.lastIndexOf url "/")]
    (subs url (inc last-slash))))

(defn- upper-case-first
  [s]
  (str (str/upper-case (subs s 0 1)) (subs s 1)))

(defn- add-post-title
  [html title]
  (let [title-without-prefix (str/trim (str/replace title #"^[a-zA-z\s]+:(.*)$" "$1"))
        title-capitalized    (upper-case-first title-without-prefix)]
    (-> html
        (prepend-to-tag :body [:h2 {} title-capitalized]))))

(defn- create-post-reference
  [url date]
  (let [date-formatted (.format (OffsetDateTime/parse date) (DateTimeFormatter/ofPattern "MMMM d, yyyy"))]
    [:p {} (link-to url "Original post") " written on " date-formatted]))

(defn- add-post-reference
  [html url date]
  (let [https-url (make-url-https url)
        p (create-post-reference https-url date)]
    (append-to-tag html :body p)))

(defn- replace-brush
  [html]
  (let [replace (fn [attr]
                  (if-let [match (and (is-attr? attr :class)
                                      (re-find #"^brush:(\w+)" (second attr)))]
                    [(first attr) (second match)]
                    attr))]
    (postwalk replace html)))

(defn- create-image-link
  [loc]
  (let [url (-> loc zip/left zip/node :href)
        download-url (if (.startsWith url "//") (str "https:" url) url)]
    {:download-url download-url
     :image-ref    (extract-image-file-name url)}))

(defn- get-image-src
  [loc]
  (map (fn [x] (create-image-link x))
       (filter (fn [x] (is-tag? (zip/node x) :img))
               (take-while (complement zip/end?) (iterate zip/next loc)))))

(defn- create-img-tag
  [loc]
  (zip/edit loc
            (fn
              [node]
              [:img {:src (str "images/" (-> node
                                             second
                                             :href
                                             extract-image-file-name))}])))

(defn replace-link-with-image-ref
  [loc]
  (if (zip/end? loc)
    (zip/node loc)
    (if (and (is-tag? (zip/node loc) :a)
             ((set (keys (second (zip/node loc)))) :href)
             (has-child-tag? loc :img))
      (recur (zip/next (create-img-tag loc)))
      (recur (zip/next loc)))))

(defn- replace-div-with-p
  [html]
  (let [replace (fn [tag] (if (and (keyword? tag)
                                   (= tag :div))
                            :p
                            tag))]
    (postwalk replace html)))

(defn- replace-h2-with-h3
  [html]
  (let [replace (fn [tag] (if (and (keyword? tag)
                                   (= tag :h2))
                            :h3
                            tag))]
    (postwalk replace html)))

(defn- replace-http-with-https
  [html]
  (let [replace (fn [attr]
                  (if (is-attr? attr #{:href :src})
                    [(first attr) (make-url-https (second attr))]
                    attr))]
    (postwalk replace html)))

(defn- make-blog-url
  [url]
  (if (str/includes? url "mrhaki.blogspot")
    (str/replace url #"(/mrhaki\.blogspot\..*?/)" "/blog.mrhaki.com/")
    url))

(defn- make-blog-url-internal
  [url post-url]
  (let [label-pattern #"^(http|https)://blog.mrhaki.com/\d+/\d+/(\w+)-.*.html$"
        label-post (nth (re-find label-pattern post-url) 2)
        label-url (nth (re-find label-pattern url) 2)]
    (if (and (not-empty label-url) (= label-post label-url))
      (str "#" (make-url-id url))
      url)))

(defn- replace-blogger-base-url
  [html]
  (let [replace (fn [attr]
                  (if (is-attr? attr :href)
                    [(first attr) (make-blog-url (second attr))]
                    attr))]
    (postwalk replace html)))

(defn- internal-link-blog-url
  [html post-url]
  (let [replace (fn [attr]
                  (if (is-attr? attr :href)
                    [(first attr) (make-blog-url-internal (second attr) post-url)]
                    attr))]
    (postwalk replace html)))

(defn fix-html
  [html title url date]
  (-> html
      html->hiccup
      replace-http-with-https
      replace-brush
      replace-div-with-p
      replace-h2-with-h3
      (add-post-title title)
      replace-blogger-base-url
      (internal-link-blog-url url)
      (add-post-reference url date)
      first
      zip/vector-zip
      replace-link-with-image-ref
      h/html))

(defn get-image-links
  [html]
  (-> html
      html->hiccup
      first
      zip/vector-zip
      get-image-src))

(comment
  (fix-html "<p>Hello world</p><pre class=\"brush:groovy\">package</pre>" "Title" "https://www.mrhaki.com" "2021-03-05T22:48:55+01:00")

  (fix-html "<p>Hello world</p><h2>Source</h2><pre>code</pre>" 
            "Title" 
            "https://blog.mrhaki.com"
            "2021-03-05T22:48:55+01:00")

  (fix-html "<p>Hello world</p><a href=\"http://blog.mrhaki.com/2021/6/gradle-goodness-other.html\">link</a>" "Title" "https://blog.mrhaki.com/2023/1/groovy-goodness-first.html" "2021-03-05T22:48:55+01:00")

  (re-matches #"^https://blog.mrhaki.com/\d+/\d+/groovy-goodness-(.*).html$"
              "https://blog.mrhaki.com/2023/1/spocklight-blog1.html")
  (second (re-find #"^https://blog.mrhaki.com/\d+/\d+/(\w+)-.*.html$"
                   "https://blog.mrhaki.com/2023/1/blog1.html"))

  (= "hello" "hello")

  (let [url "http://b2.blogspot.com"]
    (if (.startsWith url "//") (str "https:" url) url))

  (-> "<p><a href=\"http://groovy.codehaus.org/Groovlets\">Groovlets</a> are Groovy scripts that are executed by a servlet. With Groovlets a user can request a Groovy script that is executed on the server and the results are displayed in a web browser. We only have to define a servlet in the <code>web.xml</code> of a Java web application, place the Groovy libraries in the web application's <code>lib</code> folder and we can execute Groovy scripts.</p><p>The Groovy script, or we can call it a Groovlet, has a couple of implicit variables we can use. For example <code>reqeust</code>, <code>response</code> to access the <code>HttpServletRequest</code> and <code>HttpServletResponse</code> objects. We have access to the session with the <code>session</code> variable. And if we want to output data we can use <code>out</code>, <code>sout</code> and <code>html</code>.</p>\n<p>Let's create a Groovy script <code>serverinfo.groovy</code>:</p>\n<pre class=\"brush:groovy\">\ndef method = request.method\n\nif (!session) {\n    session = request.getSession(true)\n}\n\nif (!session.groovlet) {\n    session.groovlet = 'Groovlets rock!'\n}\n\nhtml.html {\n    head {\n        title 'Groovlet info'\n    }\n    body {\n        h1 'General info'\n        ul {\n            li &quot;Method: ${method}&quot;\n            li &quot;RequestURI: ${request.requestURI}&quot;\n            li &quot;session.groovlet: ${session.groovlet}&quot;\n            li &quot;application.version: ${context.version}&quot;\n        }\n        \n        h1 'Headers'\n        ul {\n            headers.each {\n                li &quot;${it.key} = ${it.value}&quot;\n            }\n        }\n    }\n}\n</pre>\n<p>A simple script to start Jetty so we can run our Groovlet:</p>\n<pre class=\"brush:groovy\">\nimport org.mortbay.jetty.Server\nimport org.mortbay.jetty.servlet.*\nimport groovy.servlet.*\n\n@Grab(group='org.mortbay.jetty', module='jetty-embedded', version='6.1.14')\ndef startJetty() {\n    def jetty = new Server(9090)\n    \n    def context = new Context(jetty, '/', Context.SESSIONS)  // Allow sessions.\n    context.resourceBase = '.'  // Look in current dir for Groovy scripts.\n    context.addServlet(GroovyServlet, '*.groovy')  // All files ending with .groovy will be served.\n    context.setAttribute('version', '1.0')  // Set an context attribute.\n    \n    jetty.start()\n}\n\nprintln &quot;Starting Jetty, press Ctrl+C to stop.&quot;\nstartJetty()\n</pre>\n<p>When we run the script to start Jetty and visit http://localhost:9090/serverinfo.groovy with our browser we get the following output:</p>\n<a href=\"http://2.bp.blogspot.com/_-vJw6r2W-bw/St1G01Hjh6I/AAAAAAAAC_8/OEN1GyBUM_0/s1600-h/blog-gg-groovlet.png\"><img style=\"display:block; margin:0px auto 10px; text-align:center;cursor:pointer; cursor:hand;width: 320px; height: 230px;\" src=\"http://2.bp.blogspot.com/_-vJw6r2W-bw/St1G01Hjh6I/AAAAAAAAC_8/OEN1GyBUM_0/s320/blog-gg-groovlet.png\" border=\"0\" alt=\"\"id=\"BLOGGER_PHOTO_ID_5394545802085631906\" /></a>"
      get-image-links)

  (-> "<a id=\"1\"/><h2>Title</h2>"
      html->hiccup)

  (replace-http-with-https [:p {} [:a {:href "http://www.mrhaki.com"}] [:img {:src "http://www.mrhaki.com"}]])
  (replace-blogger-base-url [:p {} [:a {:href "http://www.mrhaki.com"}] [:img {:src "http://www.mrhaki.com"}]])

  (defn make-blog-url
    [url]
    (if (str/includes? url "mrhaki.blogspot")
      (str/replace url #"(/mrhaki\.blogspot\..*?/)" "/blog.mrhaki.com/")
      url))

  (make-blog-url-internal
   "https://blog.mrhaki.com/2023/3/groovy-goodness-blog1.html"
   "https://blog.mrhaki.com/2023/3/groovy-goodness-blog1.html")
  (make-blog-url "http://mrhaki.blogspot.com/2009/1/sample")
  (make-blog-url "http://mrhaki.blogspot.nl/2020/01/01/groovy.html")
  (make-blog-url "https://mrhaki.blogspot.nl/2012/06/groovy-goodness-revisited-getting-sum.html")
  (last [1 2 3])

  (make-url-https "http://test")
  (make-url-https "https://test")

  (defn make-url-id
    [url]
    (let [last-index-slash (inc (.lastIndexOf url "/"))
          index-ext (.lastIndexOf url ".html")]
      (subs url last-index-slash index-ext)))

  (re-find #"^(http|https)://(.*)/(\d+)/(\d+)/(.*).html$" "https://mrhaki.blogspot.nl/2012/06/groovy-goodness-revisited-getting-sum.html")

  (re-matches #"^http://(.*).html$" "http://mrhaki.blogspot.nl/2012/06/groovy-goodness-revisited-getting-sum.html")

  (make-url-id "https://mrhaki.blogspot.nl/2012/06/groovy-goodness-revisited-getting-sum.html")

  (str/trim (str/replace "Clojure Goodness: Test" #"^[a-zA-z\s]+:(.*)$" "$1"))

  (#{:href :src} :src)

  (or (and (set? #{:href :src})
           (#{:href :src} :p))
      ((= :p :p)))

  (replace-brush (-> "<pre class=\"brush:groovy;light;true\">package mrhaki;</pre>"
                     html->hiccup))

  (postwalk (fn [x] (println x) x) (-> "<div><p class=\"a\"><pre class=\"brush:groovy;\">package mrhaki;</pre></div>"
                                       html->hiccup))

  (when-let [match (re-find #"^brush:(\w+)" "brush:java;light:true")]
    (second match))

  (if-let [match (and (= :class (first [:clazz "brush"]))
                      (re-find #"^brush:(\w+)" "brush:java;light:true"))]
    [:class (second match)]
    [])

  (.contains (second [:class "brush1:groovy"]) "brush:")

  (-> "<p>Hello world</p>"
      html->hiccup
      (add-post-title "Title")
      (h/html))

  (let [[before after] (split-at 2 [:body {} [:p {} "Hello world"]])]
    (vec (concat before [[:h1 {} "Title"]] after)))

  (def blog-hiccup (-> "<p>Hello world</p>"
                       html->hiccup))

  (let [replace (fn [x] (if (and (keyword? x)
                                 (= x :div)) :p x))]
    (postwalk replace [:div {} [:a {:href "a"}]]))

  (prepend-to-tag blog-hiccup :body [:h1 {} "Title"])
  (assoc [:body {} [:p {} "Hello world"]] 1 [:h1 {} "Title"])
  (count [:body {} [:p {} "Hello world"]])

  (def d "2021-03-05T22:48:55+01:00")

  (.format (OffsetDateTime/parse d) (DateTimeFormatter/ofPattern "MMMM d, yyyy"))

  (html->hiccup "<p>Hello</p>")
  (append-to-tag (html->hiccup "<p>Hello</p>") :body [:p {} "new"])
  (prepend-to-tag (html->hiccup "<p>Hello</p>") :body [:p {} "new"])


  (postwalk
   (fn [x] (println x) x)
   [:html [:body [:h1 {:class "test"} "Title"] [:p "First paragraph"]]])

  (clojure.walk/postwalk
   (fn [x] (println x) x)
   sample-hiccup)

  (link-to "http://www.mrhaki.com" "blog")
  (h/html *1)

  (upper-case-first "hello")
  (add-post-title blog-hiccup "Title")
  (add-post-reference sample-hiccup "http://www.mrhaki.com" "2021-03-05T22:48:55+01:00")

  (is-attr? [:p "brush:groovy"] :class)

  (is-tag? [:body {} "test"] :body)

  (defn is-body?
    [x]
    (and (vector? x)
         (keyword? (first x))
         (= :body (first x))))

  (is-body? [:body [:p "test" [:a {:href "/"} "test"]]])

  (conj [:body [:h1 "test"] [:p "test"]] [:p "ref"])

  (defn append-to-body
    [html href date]
    (let [edit (fn [x]
                 (if (is-tag? x :body)
                   (conj x [:p [:a {:href href} "Original post"] " written on " date])
                   x))]
      (clojure.walk/postwalk edit html)))

  (defn prepend-to-body
    [html title]
    (let [edit (fn [x]
                 (if (is-tag? x :body)
                   (assoc x 2 [:h1 title])
                   x))]
      (clojure.walk/postwalk edit html)))

  (prepend-to-body sample-hiccup "Blog title")
  (prepend-to-body [:html [:body {} [:p {} "test"]]] "Blog title")
  (apply conj (rest (vec (rest [:body {} [:p "test"] [:div [:p "div"]]]))))
  (def body [:body {} [:p "test"] [:div [:p "div"]]])
  (apply conj (rest (vec (rest body))))
  [(first body) (second body) [:h1 "title"] (apply conj (rest (vec (rest body))))]
  (apply conj (vec (rest [:body {} [:p "test"] [:div [:p "div"]]])))
  (conj [:body [:h1 "title"]] (apply conj (rest [:body [:p "test"] [:div [:p "div"]]])))
  (apply conj (drop 2 body))
  (apply conj (nnext body))
  (conj [:body {}] (apply conj (drop 2 body)))
  (apply conj (rest (vec (rest body))))
  (split-at 2 body)
  (apply conj [:a :b] :q [:c :d])
  (let [[before after] (split-at 2 body)]
    (apply conj after [:h1 {} "Title"] before))
  (concat [:a :b] [[:c]] [[:d :e]])
  (let [[before after] (split-at 2 body)]
    (vec (concat before [[:h1 {} "Title"]] after)))

  (insert-at body 2 [:h1 {} "test"])

  (assoc [0 1 2 3 4] 2 "test")
  (assoc body 2 [:h1 "title"])
  (cons [:h1 "title"] body)

  (append-to-body sample-hiccup "href-link" "Today")
  (h/html (append-to-body sample-hiccup "href-link" "Today"))

  (postwalk (fn [x] (if (and (is-tag? x :div)
                             (contains? (second x) :class)
                             (contains? (second x) :style))
                      [:p (rest x)]
                      x)) (html->hiccup post-with-image))

  (postwalk (fn [x] (if (and (is-tag? x :a)
                             (contains? (second x) :href)
                             (contains? (second x) :imageanchor))
                      [:img {:src (:href (second x))}]
                      x)) (html->hiccup post-with-image))
  (postwalk (fn [x] (println x) x) (html->hiccup post-with-image))
  (html->hiccup post-with-image)

  (defn replace-image-ref
    [html]
    (let [replace (fn [tag] (if (and (is-tag? tag :a)
                                     (contains? (second tag) :href)
                                     (some #{:imageanchor :onblur} (keys (second tag))))
                              [:img {:src (str "images/" (-> tag second :href extract-image-file-name))}]
                              tag))]
      (postwalk replace html)))

  (defn fast-forward-z [loc]
    (if (zip/end? loc)
      loc
      (recur (zip/next loc))))

  (defn has-child-tag?
    [loc tag]
    (-> loc zip/down zip/right zip/right zip/node (is-tag? tag)))


  (-> post-with-image
      html->hiccup
      first
      zip/vector-zip)

  ;;[:img {:src (str "images/" (-> tag second :href extract-image-file-name))}]
  (defn create-img-tag
    [loc]
    (println loc)
    (println (zip/node loc))
    (println (keys (second (zip/node loc))))
    (println ((set (keys (second (zip/node loc)))) :href))
    (zip/edit loc (fn [node] [:img {:src (str "images/" (-> node second :href extract-image-file-name))}])))

  (defn replace-link-with-image-ref
    [loc]
    (if (zip/end? loc)
      (zip/node loc)
      (if (and (is-tag? (zip/node loc) :a)
               ((set (keys (second (zip/node loc)))) :href)
               (has-child-tag? loc :img))
        (recur (zip/next (create-img-tag loc)))
        (recur (zip/next loc)))))

  (defn replace-link-with-image-ref
    [loc]
    (if (zip/end? loc)
      (zip/node loc)
      (if (and (is-tag? (zip/node loc) :a)
               ((set (keys (second (zip/node loc)))) :href))
        (recur (zip/next (create-img-tag loc)))
        (recur (zip/next loc)))))

  ((set (keys {:href "h" :style "s"})) :href)

  (defn replace-link-with-image-ref
    [loc]
    (if (zip/end? loc)
      (zip/node loc)
      (if  (is-tag? (zip/node loc) :a)
        (recur (zip/next (create-img-tag loc)))
        (recur (zip/next loc)))))

  (-> post-with-image
      html->hiccup
      first
      zip/vector-zip
      replace-link-with-image-ref)

  (get-image-links post-with-image)

  (def post-with-image "<p>The best IDE to use when developing Groovy code is <a href=\"http://www.jetbrains.com\">IntelliJ IDEA</a>.</p><div class=\"separator\" style=\"clear: both; text-align: center;\"><a href=\"http://4.bp.blogspot.com/-R7NV9p4epZA/VflKi2NVP3I/AAAAAAAALTU/x2IyUiES6-g/s1600/idea-intention-convert-string.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" src=\"http://4.bp.blogspot.com/-R7NV9p4epZA/VflKi2NVP3I/AAAAAAAALTU/x2IyUiES6-g/s640/idea-intention-convert-string.png\" /></a></div><p style=\"clear:both\">When we select <em>Convert to String</em> IntelliJ IDEA changed our value assignment:</p><div class=\"separator\" style=\"clear: both; text-align: center;\"><a href=\"http://1.bp.blogspot.com/-9qsqVoEDhMM/VflLErnPxeI/AAAAAAAALTc/s5NjfIfD-5M/s1600/idea-intention.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" src=\"http://1.bp.blogspot.com/-9qsqVoEDhMM/VflLErnPxeI/AAAAAAAALTc/s5NjfIfD-5M/s640/idea-intention.png\" /></a></div><p style=\"clear:both;\">Written with Groovy 2.4.4 and IntelliJ IDEA CE 14.1.4</p>")

  (get-image-links post-with-image)

  (defn get-parent-href
    [loc]
    (zip/node (zip/up loc)))

  (-> [:a {:href "h" :imageanchor "1"} [:img {:src "s" :border "0"}]]
      zip/vector-zip
      zip/down
      zip/right
      zip/right
      zip/down
      zip/right
      zip/up
      zip/left
      zip/node
      :href)

  (defn get-image-src
    [loc]
    (map (fn [x] (-> x zip/left zip/node :href))
         (filter (fn [x] (is-tag? (zip/node x) :img))
                 (take-while (complement zip/end?) (iterate zip/next loc)))))

  (defn get-image-src
    [loc]
    (filter (fn [x] (is-tag? (zip/node x) :img))
            (take-while (complement zip/end?) (iterate zip/next loc))))

  (-> [:a {:href "h" :imageanchor "1"} [:img {:src "s" :border "0"}]]
      zip/vector-zip
      get-image-src)

  (-> [:html {}
       [:head {}]
       [:body
        {}
        [:p
         {}
         "When we make some small changes to "
         [:code {} "stylesheet.css"]
         " and run the "
         [:code {} "groovydoc"]
         " task we see the following example output:"]
        [:div
         {:class "separator", :style "clear: both; text-align: center;"}
         [:a
          {:href        "http://4.bp.blogspot.com/-FVvq5yl-gfw/VqdVo7EHfRI/AAAAAAAALc4/mNdUCKrFsv4/s1600/groovydoc.png",
           :imageanchor "1",
           :style       "margin-left: 1em; margin-right: 1em;"}
          [:img
           {:border "0",
            :src    "http://4.bp.blogspot.com/-FVvq5yl-gfw/VqdVo7EHfRI/AAAAAAAALc4/mNdUCKrFsv4/s640/groovydoc.png"}]]]
        [:p
         {:style "clear:both;"}
         "Notice we have our custom header, the Gravatar image and custom CSS."]
        [:p {} "Written with Groovy 2.4.5."]]]
      zip/vector-zip
      get-image-src)

  (-> post-with-image
      html->hiccup
      first
      zip/vector-zip
      get-image-src)

  (defn find-image-ref
    [html]
    (let [find-img-src (fn [el]
                         (println el)
                         (if (is-attr? el :src)
                           [:download-url (second el)]
                           el))]
      (postwalk find-img-src html)))
  (find-image-ref [:p [:img {:src "http://blog.mrhaki.com"}]])



  (-> [:p [:img {:src "a"}] [:img {:src "b"}]]
      clojure.zip/vector-zip
      get-image-src)


  (-> [:p [:img {:src "http://blog.mrhaki.com"}]]
      clojure.zip/vector-zip
      clojure.zip/down
      clojure.zip/right
      clojure.zip/node)

  (defn fast-forward [loc]
    (if (zip/end? loc)
      (zip/node loc)
      (recur (zip/next loc))))

  (defn first-keyword [loc]
    (if (zip/end? loc)
      loc
      (if (keyword? (zip/node loc))
        (recur (zip/node loc))
        (recur (zip/next loc)))))

  (fast-forward (zip/vector-zip [:p [:img {:src "http://blog.mrhaki.com"}]]))
  (first-keyword (zip/vector-zip [:p [:img {:src "http://blog.mrhaki.com"}]]))

  (fix-html post-with-image "title" "" "2021-03-05T22:48:55+01:00")

  (let [x [:a {:href "" :imageanchor "" :style ""}]]
    (and (is-tag? x :a)
         (contains? (second x) :imageanchor)))

  (#{:href} :href)
  (contains? {:href "" :imageanchor ""} :imageanchor)

  (defn fix-brush
    [html]
    (let [edit (fn [x]
                 (if (and (is-attr? x :class)
                          (.contains (second x) "brush:"))
                   {:class ""}
                   x))]
      (clojure.walk/postwalk edit html)))

  (fix-brush [:pre {:class "brush1:groovy"} ""])
  (.contains "brush:groovy" "brush")
  (clojure.string/includes? "brush:java" "brush")

  (defn add-root-to-html
    [root-path html]
    (let [edit (fn [x]
                 (if (key-value? x)
                   [(first x) root-path]
                   x))]
      (clojure.walk/postwalk edit html)))

  (add-root-to-html "/root" [:p {}
                             [:a {:href "about.html" :title "About"} "See about"]])

  (h/html (concat [:p {} [:a {:href "/"}]] [:p [:a {:href "/new"} "Original post"]]))
  (h/html [:div [:p {} "One"] [:p {} "test" [:a {:href "/"}] "smaple"]])

  (h/html [:html [:body [:h1 "Title"] [:p "First paragraph"]]])

  (apply #{:onblur :image} [:image])


  (some #{:k :d} (keys (apply hash-map [:b "abc" :c "def" :d "def" :k "klm"])))

  (some #{:imageanchor :onblur} (keys (apply hash-map [:onblur "blur" :href "href"])))
  (set (keys {:a "" :href "" :onblur ""}))
  (apply inc [1 2 3])
  ;;(some #{:onblur :imageanchor} (keys (apply hash-map (second tag))))
  (defn replace-image-ref
    [html]
    (let [replace (fn [tag] (if (and (is-tag? tag :a)
                                     (contains? (second tag) :href)
                                     (some #{:imageanchor :onblur} (keys (second tag))))
                              [:img {:src (str "images/" (-> tag second :href extract-image-file-name))}]
                              tag))]
      (postwalk replace html)))

  (-> alternate-image-html
      html->hiccup
      replace-link-with-image-ref)

  (def alternate-image-html "<p>In the <code>groovy.servlet</code> package we can find the class <code>ServletCategory</code>. We can use this class to access attributes on the servlet context, page context, request and session with the dot notation. The methods <code>putAt</code> and <code>getAt</code> are implemented in this class for all four objects. If we write Groovy code in the context of these object we can use the <code>ServletCategory</code> to make the code more Groovy.</p>\n<p>In the following sample we write two servlets that use the <code>ServletCategory</code> to write and read attribute values. We compile the code to Java classes to get executable servlet. Finally we write a Groovy script to run Jetty with our servlets.</p>\n<pre class=\"brush:groovy\">\n// File: Start.groovy\nimport javax.servlet.http.*\nimport javax.servlet.*\nimport groovy.servlet.ServletCategory\n\nclass Start extends HttpServlet {\n    def application \n    \n    void init(ServletConfig config) {\n        super.init(config)\n        application = config.servletContext\n        use(ServletCategory) {\n            application.author = 'mrhaki'\n        }\n    }\n    \n    void doGet(HttpServletRequest request, HttpServletRespons response) {\n        def session = request.session\n        use (ServletCategory) {\n            if (session.counter) {  // We can use . notation to access session attribute.\n                session.counter++  // We can use . notation to set value for session attribute.\n            } else {\n                session.counter = 1\n            }\n            \n            request.pageTitle = 'Groovy Rocks!'\n        }\n        application.getRequestDispatcher('/output').forward(request, response)\n    }\n}\n</pre>\n<pre class=\"brush:groovy\">\n// File: Output.groovy\nimport javax.servlet.http.*\nimport javax.servlet.*\nimport groovy.xml.*\nimport groovy.servlet.ServletCategory\n\nclass Output extends HttpServlet {\n    def context \n    \n    void init(ServletConfig config) {\n        super.init(config)\n        context = config.servletContext\n    }\n    \n    void doGet(HttpServletRequest request, HttpServletRespons reponse) {\n        def html = new MarkupBuilder(response.writer)\n        def session = request.session\n        \n        use(ServletCategory) {\n            html.html {\n                head {\n                    title request.pageTitle\n                }\n                body {\n                    h1 request.pageTitle \n                    h2 \"$context.version written by $context.author\"\n                    p \"You have requested this page $session.counter times.\"\n                }\n            }\n        }\n        \n    }\n}\n</pre>\n<pre class=\"brush:groovy\">\n// File: run.groovy\nimport org.mortbay.jetty.*\nimport org.mortbay.jetty.servlet.*\nimport groovy.servlet.*\n\n@Grab(group='org.mortbay.jetty', module='jetty-embedded', version='6.1.14')\ndef startJetty() {\n    def jetty = new Server(9090)\n    def context = new Context(jetty, '/', Context.SESSIONS)\n    context.resourceBase = '.'\n    context.addServlet Start, '/start'\n    context.addServlet Output, '/output'\n    context.setAttribute 'version', '1.0'\n    jetty.start()\n}\n\nstartJetty()\n</pre>\n<p>In our web browser we open http://localhost:9090/start and get the following output:</p>\n<a onblur=\"try {parent.deselectBloggerImageGracefully();} catch(e) {}\" href=\"http://4.bp.blogspot.com/_-vJw6r2W-bw/SxWK_kdeZDI/AAAAAAAADKc/rcGij0DlXvo/s1600/blog-gg-servletcategory.png\"><img style=\"display:block; margin:0px auto 10px; text-align:center;cursor:pointer; cursor:hand;width: 320px; height: 250px;\" src=\"http://4.bp.blogspot.com/_-vJw6r2W-bw/SxWK_kdeZDI/AAAAAAAADKc/rcGij0DlXvo/s320/blog-gg-servletcategory.png\" border=\"0\" alt=\"\"id=\"BLOGGER_PHOTO_ID_5410383352079213618\" /></a>")


  (def sample-hiccup [:html
                      {}
                      [:head {}]
                      [:body
                       {}
                       [:p
                        {}
                        "In Groovy we can use the "
                        [:code {} "@Delegate"]
                        " annotation "
                        [:a
                         {:href "http://mrhaki.blogspot.com/2009/08/groovy-goodness-delegate-to-simplify.html"}
                         "for some time now"]
                        " to automatically include methods from a delegated class into our own class. When the method is invoked on our class, it will be delegated to the correct class. Since Groovy 2.2 we can use the "
                        [:code {} "includes"]
                        " and "
                        [:code {} "excludes"]
                        " annotation parameters to define which methods of the delegated class we want to use in our own class. If any required method from the delegated class, which can also be defined in an interface, is left out when we use "
                        [:code {} "includes"]
                        " and "
                        [:code {} "excludes"]
                        ", then we get a compilation error, telling us to implement the required method from the delegated class or interface."]
                       [:p
                        {}
                        "In the following sample we define a "
                        [:code {} "SimpleEvent"]
                        " class with a "
                        [:code {} "start"]
                        " and "
                        [:code {} "end"]
                        " property we delegate to the "
                        [:code {} "Date"]
                        " class. For the "
                        [:code {} "start"]
                        " property we don't want the "
                        [:code {} "after()"]
                        " method to be delegated, because we want to delegate it to the "
                        [:code {} "Date"]
                        " object of the "
                        [:code {} "end"]
                        " property. And for the "
                        [:code {} "end"]
                        " property we want to delegate the "
                        [:code {} "before()"]
                        " delegated method of the "
                        [:code {} "Date"]
                        " object of the "
                        [:code {} "start"]
                        " property."]
                       [:pre
                        {:class "brush:groovy"}
                        "@groovy.transform.ToString
                         class SimpleEvent {
                             @Delegate(excludes=['after']) Date start
                             @Delegate(excludes=['before']) Date end
                             String description
                         }
                         
                         def event = new SimpleEvent(description: 'Groovy seminar',
                                                     start: nextWeek, 
                                                     end: nextWeek + 3)
                         
                         // Delegate to Date start property.
                         assert event.before(nextWeek + 1)
                         assert !event.before(nextWeek - 1)
                         assert !event.before(today)
                         
                         // Delegate to Date end property.
                         assert event.after(today)        
                         assert event.after(nextWeek + 1)     
                         assert !event.after(nextWeek + 4)        
                         
                         
                         // Helper get methods:
                         def getToday() {
                             new Date().clearTime()
                         }
                         
                         def getNextWeek() {
                             today + 7
                         }
                         "]
                       [:p {} "Code written with Groovy 2.2."]]]))
