(ns blogger-leanpub-converter.manuscript
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]))

(defn read-manuscript
  [base-dir name]
  (edn/read-string (slurp (io/file base-dir name))))

(comment

  (read-manuscript "manuscripts" "clojure-goodness.edn")
  (read-manuscript "manuscripts" "groovy-goodness.edn")

  (def manuscript {:title "Clojure Goodness Notebook"
                   :chapters [{:title "Core"
                               :posts ["http://blog.mrhaki.com/2020/07/clojure-goodness-replacing-characters.html"]}]})
  
  ,)
