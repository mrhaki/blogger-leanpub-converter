(ns blogger-leanpub-converter.storage
  (:require [datalevin.core :as d]))

(defn get-post-by-url
  "Get blog posts from database by given url."
  [conn url]
  (into {} (d/entity @conn [:url url])))

(defn get-connection
  "Get Datalevin database connection"
  ([dir schema] 
   (d/get-conn dir schema))
  ([config]
   (d/get-conn (:database-dir config) (:schema config))))

(defn close-connection
  "Close Datalevin database connection"
  [conn]
  (if (d/closed? conn)
    nil
    (d/close conn)))
  
(comment
  (def config {:database-dir "/Users/mrhaki/Documents/blogger-db"
               :schema {:id {:db/valueType :db.type/string
                             :db/unique :db.unique/identity}
                        :url {:db/valueType :db.type/string
                              :db/unique :db.unique/identity}
                        :labels {:db/valueType :db.type/string
                                 :db/cardinality :db.cardinality/many}}})
  (def conn (get-connection config))
  (prn conn)
  (close-connection conn)
  
  (get-post-by-url conn "http://blog.mrhaki.com/2018/06/groovy-goodness-tuples-with-up-to-9.html")
  
  (d/q '[:find ?url
         :where 
         [?e :url ?url]]
       @conn)

  (get-post-by-url conn "http://blog.mrhaki.com/2020/07/clojure-goodness-replacing-characters.html")
  (type *1)
  (def post (get-post-by-url conn "http://blog.mrhaki.com/2020/07/clojure-goodness-replacing-characters.html"))
  
  (println post)
  (prn (:content post))
  
  (println (:labels (get-post-by-url conn "http://blog.mrhaki.com/2020/07/clojure-goodness-replacing-characters.html")))

  (:url (d/entity @conn [:url "http://blog.mrhaki.com/2021/03/gradle-goodness-enabling-preview.html"]))

  (def u "http://blog.mrhaki.com/2021/03/gradle-goodness-enabling-preview.html")
  (d/entity @conn [:url u])
  
  (d/entity @conn 1367)
  (into {} *1)
  (println *2)

  (d/datoms @conn :eav 11)

  (d/q '[:find ?e
         :where
         [?e :id "7356010244958013577"]]
       @conn)

  (d/q '[:find ?e :in $ ?id :where [?e :id ?id]] @conn "a")

  (:labels (d/entity @conn 11))
  (:url (d/entity @conn [:id "b"]))

  (def id "7356010244958013577")
  (d/entity @conn [:id id])
  (keys *1)
  (:db/id *2)

  (type *1)
  (into {} *2)

  (d/q '[:find (pull ?e [*])
         :where
         [?e :id "7393203034235579281"]]
       @conn)
  
  (d/q '[:find (pull ?e [*])
         :where
         [?e :url "http://blog.mrhaki.com/2018/06/groovy-goodness-tuples-with-up-to-9.html"]]
       @conn)
  
  (d/q '[:find (pull ?e [*])
         :where
         [?e :title "Gradle Goodness: Enabling Preview Features For Java"]]
       @conn)

  (def entity-id 1356)
  (d/q '[:find ?e
         :where
         [?e :db/id "1356"]]
       @conn)

  (d/q '[:find ?id
         :where [_ :id ?id]]
       @conn)

  (d/q '[:find ?id ?url
         :where
         ;;[?e :title "Gradle Goodness: Enabling Preview Features For Java"]
         [?e :labels "Groovy"]
         [?e :id ?id]
         [?e :url ?url]]
       @conn)

  (d/q '[:find (count ?e)
         :where [?e :labels "Java"]]
       @conn)

  (def blogger-post {:id "7356010244958013577",
                     :published "2021-03-05T22:44:00+01:00",
                     :updated "2021-03-05T22:48:55+01:00",
                     :url "http://blog.mrhaki.com/2021/03/gradle-goodness-enabling-preview.html",
                     :title "Gradle Goodness: Enabling Preview Features For Java",
                     :content "<p>Java introduced preview features in the language since Java 12.
                    This features can be tried out by developers, but are still subject to change and can even be removed in a next release.
                    By default the preview features are not enabled when we want to compile and run our Java code.
                    We must explicitly specify that we want to use the preview feature to the Java compiler and Java runtime using the command-line argument <code>--enable-preview</code>.
                    In Gradle we can customize our build file to enable preview features.
                    We must customize tasks of type <code>JavaCompile</code> and pass <code>--enable-preview</code> to the compiler arguments.
                    Also tasks of type <code>Test</code> and <code>JavaExec</code> must be customized where we need to add the JVM argument <code>--enable-preview</code>.</p>
                    <p>Written with Gradle 6.8.3</p>
                    ",
                     :labels ["Gradle"
                              "Gradle 6.8.3"
                              "Gradle:Goodness"
                              "GradleGoodness:Java"
                              "GradleGoodness:Kotlin"],
                     :etag "\"dGltZXN0YW1wOiAxNjE0OTgwOTM1MzI4Cm9mZnNldDogMzYwMDAwMAo\""})

  
  ,)

