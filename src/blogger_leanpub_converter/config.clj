(ns blogger-leanpub-converter.config
  (:require [cprop.core :refer [load-config]]))

(defn get-config
  "Get configuration for a given book."
  [book & {:keys [config-file] :or {config-file "~/.blogger.edn"}}]
  (let [config (load-config :file config-file)
        notebook (str book "-notebook")]
    {:database-dir (-> config :database :storage-dir)
     :manuscript   (str notebook ".edn")
     :output-dir   (str "leanpub/" notebook)})) 

(comment

  (load-config :file "~/.blogger.edn")

  (get-config "-b" "test-book")

  (def test-book (get-config "test-book"))
  test-book
  
  ,)