(ns blogger-leanpub-converter.core
  (:require [blogger-leanpub-converter.storage :as st]
            [blogger-leanpub-converter.html :as html]
            [blogger-leanpub-converter.pandoc :refer [html->markdown_leanpub]]
            [blogger-leanpub-converter.markdown :refer [fix-code-block fix-section-title]]
            [blogger-leanpub-converter.manuscript :refer [read-manuscript]]
            [blogger-leanpub-converter.leanpub :as leanpub]
            [blogger-leanpub-converter.application :as app]
            [blogger-leanpub-converter.config :as config]
            [datalevin.core :as d]
            [clojure.string :as str]
            [com.stuartsierra.component :as component]
            [blogger-leanpub-converter.cli :as cli])
  (:gen-class))


(defn url->post
  [conn url]
  (st/get-post-by-url conn url))

(defn post->image-links
  [post]
  (assoc post :image-links (vec (html/get-image-links (:content post)))))

(defn get-chapter-posts
  [conn chapter]
  (assoc chapter :posts (mapv (fn [url] (->> url (url->post conn) post->image-links)) (:posts chapter))))

(defn post->markdown
  [post]
  (let [{:keys [content title url published]} post]
    (assoc post :markdown (-> (html/fix-html content title url published)
                              html->markdown_leanpub
                              fix-code-block
                              (fix-section-title url)))))

(defn get-chapter-posts-markdown
  [chapter]
  (assoc chapter :posts (mapv (fn [post] (post->markdown post)) (:posts chapter))))

(defn chapter->markdown
  [chapter]
  (let [{:keys [title posts]} chapter
        markdown (map :markdown posts)]
    (assoc chapter :markdown (str "# " title \newline \newline (str/join \newline markdown)))))

(defn manuscript->markdown
  [conn manuscript]
  (assoc manuscript :chapters (mapv (fn [chapter] (->> chapter
                                                       (get-chapter-posts conn)
                                                       get-chapter-posts-markdown
                                                       chapter->markdown)) (:chapters manuscript))))
(defn -main
  "Read manuscript and transform to Leanpub markdown"
  [& args]
  (let [{:keys [exit-message ?ok book]} (cli/validate-args args)]
    (if exit-message
      (cli/exit (if ?ok 0 1) exit-message)
      (let [config (config/get-config book)
            output-dir (:output-dir config)
            system (component/start (app/production-system config))
            database (:database system)
            conn (:connection database)
            manuscript (:manuscript config)
            notebook (manuscript->markdown conn (read-manuscript "manuscripts" manuscript))]
        (leanpub/save-sample! output-dir notebook)
        (leanpub/save-book! output-dir notebook)
        (component/stop system)))))

(comment

  (-main)



  (def config {:database-dir    "/Users/mrhaki/Documents/blogger-db"
               :schema {:id     {:db/valueType :db.type/string
                                 :db/unique    :db.unique/identity}
                        :url    {:db/valueType :db.type/string
                                 :db/unique    :db.unique/identity}
                        :labels {:db/valueType   :db.type/string
                                 :db/cardinality :db.cardinality/many}}})
  (def system (component/start (app/production-system config)))

  ;; (def conn (st/get-connection config))
  (def conn (st/get-connection (:database-dir config) (:schema config)))
  (prn conn)
  (d/close conn)
  (d/conn? conn)

  (st/close-connection conn)
  (into {} (st/get-post-by-url (:connection conn) "http://blog.mrhaki.com/2020/07/clojure-goodness-replacing-characters.html"))

  (st/get-post-by-url conn "http://blog.mrhaki.com/2011/11/groovy-goodness-magic-package-to-add.html")
  (prn (:content (st/get-post-by-url conn "http://blog.mrhaki.com/2009/12/groovy-goodness-using-servletcategory.html")))
  (html/get-image-links (:content (st/get-post-by-url conn "http://blog.mrhaki.com/2009/12/groovy-goodness-using-servletcategory.html")))

  (post->markdown {:content "tst"})

  (def post (st/get-post-by-url conn "http://blog.mrhaki.com/2018/06/groovy-goodness-tuples-with-up-to-9.html"))
  (def post-html (html/fix-html (:content post) (:title post) (:url post) (:published post)))
  (prn (html->markdown_leanpub post-html))

  (def app (app/example-system {:data-dir "/Users/mrhaki/Documents/blogger-db"}))
  (def running-app (component/start app))
  (def conn (:connection (:database running-app)))
  (:database (component/stop app))

  (def post (st/get-post-by-url conn "http://blog.mrhaki.com/2015/09/groovy-goodness-intellij-idea.html"))
  (prn post)
  (prn (:content post))
  (-> (:content post)
      html/html->hiccup)


  (get-in {:posts [{:url "a"} {:url "b"}]} [:posts])
  (map :url (:posts {:posts [{:url "a"} {:url "b"}]}))


  (prn (:content (st/get-post-by-url conn "http://blog.mrhaki.com/2009/10/groovy-goodness-groovlets-as.html")))

  (read-manuscript "manuscripts" "clojure-goodness.edn")
  (manuscript->markdown conn (read-manuscript "manuscripts" "clojure-goodness.edn"))
  (def groovy-notebook (manuscript->markdown conn (read-manuscript "manuscripts" "groovy-goodness.edn")))

  (leanpub/save-sample! "sample" (:chapters groovy-notebook))
  (leanpub/save-book! {:output-dir "sample/groovy-notebook"} groovy-notebook)

  (def clojure-notebook (manuscript->markdown conn (read-manuscript "manuscripts" "clojure-goodness.edn")))
  (leanpub/save-book! {:output-dir "sample/clojure-notebook"} clojure-notebook)

  (def spock-framework-notebook (manuscript->markdown conn (read-manuscript "manuscripts" "spocklight-notebook.edn")))
  (:chapters spock-framework-notebook)
  (leanpub/save-book! "sample/spocklight-notebook" spock-framework-notebook)

  (def awesome-asciidoctor-notebook (manuscript->markdown conn (read-manuscript "manuscripts" "awesome-asciidoctor.edn")))
  (leanpub/save-book! {:output-dir "sample/awesome-asciidoctor-notebook"} awesome-asciidoctor-notebook)

  (def gradle-goodness-notebook (manuscript->markdown conn (read-manuscript "manuscripts" "gradle-goodness.edn")))
  (leanpub/save-book! {:output-dir "sample/gradle-goodness-notebook"} gradle-goodness-notebook)

  (map #((juxt :url :image-links) %) (:posts (nth (:chapters gradle-goodness-notebook) 3)))

  (println awesome-asciidoctor-notebook)

  (filter #(= "Groovy Goodness: Groovlets as Lightweight Servlets" (:title %)) (:posts (nth (:chapters groovy-notebook) 18)))

  (println groovy-notebook)

  (leanpub/save-chapter! "sample" "Syntax" "")

  (def posts ["http://blog.mrhaki.com/2020/07/clojure-goodness-replacing-characters.html"
              "http://blog.mrhaki.com/2021/03/gradle-goodness-enabling-preview.html"])

  (get-chapter-posts-markdown (:posts (get-chapter-posts conn {:title "h1" :posts posts})))
  (get-chapter-posts conn {:title "h1" :posts posts})

  (def leanpub-config {:output-dir "./sample"})

  (chapter->markdown chapter)

  (get-chapter-posts conn chapter)
  (def chapter {:title "Syntax",
                :posts ["## Groovy Goodness: Default Imports
                      
                      Groovy allows to write dense code. For example we don\\'t have to write explicit `import` statements for a lot of every day packages. The following packages are imported by default in Groovy:
                      
                      {lang=text}
                      ~~~
                      java.io.* 
                      java.lang.* 
                      java.math.BigDecimal 
                      java.math.BigInteger 
                      java.net.* 
                      java.util.* 
                      groovy.lang.* 
                      groovy.util.* 
                      ~~~
                      
                      So we can write code like this without an import statement:
                      
                      {lang=text}
                      ~~~
                      def now = new Date()
                      def file = new File(\".\")
                      def url = new URL('http://mrhaki.blogspot.com')
                      def list = new ArrayList()
                      ~~~
                      
                      [Original post](https://blog.mrhaki.com/2009/10/groovy-goodness-default-imports.html) written on October 15, 2009
                      "
                        "## Groovy Goodness: Semicolons are Optional
                         
                         In Groovy we can leave out semicolons at the end of the line. As long as we use one statement per line. If we use multiple statements on a line we must use a semicolon to separate the statements.
                         
                         {lang=text}
                         ~~~
                         assert true
                         assert !false
                         
                         assert true; assert !false  // Need semicolon to separate statements.
                         
                         assert true;  // Optional, so we can use them at the end of the line.
                         assert !false;
                         ~~~
                         
                         [Original post](https://blog.mrhaki.com/2009/12/groovy-goodness-semicolons-are-optional.html) written on December 8, 2009
                         "]})

  (post->markdown blogger-post)

  (def chapter-markdown (get-chapter-posts conn {:title "h1"
                                                 :posts ["http://blog.mrhaki.com/2020/07/clojure-goodness-replacing-characters.html"]}))

  (get-chapter-posts conn {:title "h1"
                           :posts ["http://blog.mrhaki.com/2020/07/clojure-goodness-replacing-characters.html"]})
  (chapter->markdown chapter-markdown)
  (get-in chapter-markdown [:posts :url])
  (mapv :markdown (:posts chapter-markdown))

  (vec (html/get-image-links (:content blogger-post)))

  (def blogger-post {:id        "7356010244958013577",
                     :published "2021-03-05T22:44:00+01:00",
                     :updated   "2021-03-05T22:48:55+01:00",
                     :url       "http://blog.mrhaki.com/2021/03/gradle-goodness-enabling-preview.html",
                     :title     "Gradle Goodness: Enabling Preview Features For Java",
                     :content   "<p>Java introduced preview features in the language since Java 12.
                    This features can be tried out by developers, but are still subject to change and can even be removed in a next release.
                    By default the preview features are not enabled when we want to compile and run our Java code.
                    We must explicitly specify that we want to use the preview feature to the Java compiler and Java runtime using the command-line argument <code>--enable-preview</code>.
                    In Gradle we can customize our build file to enable preview features.
                    We must customize tasks of type <code>JavaCompile</code> and pass <code>--enable-preview</code> to the compiler arguments.
                    Also tasks of type <code>Test</code> and <code>JavaExec</code> must be customized where we need to add the JVM argument <code>--enable-preview</code>.</p>
                    <p>Written with Gradle 6.8.3</p>
                    ",
                     :labels    ["Gradle"
                                 "Gradle 6.8.3"
                                 "Gradle:Goodness"
                                 "GradleGoodness:Java"
                                 "GradleGoodness:Kotlin"],
                     :etag      "\"dGltZXN0YW1wOiAxNjE0OTgwOTM1MzI4Cm9mZnNldDogMzYwMDAwMAo\""})

  (def post-with-image (st/get-post-by-url conn "http://blog.mrhaki.com/2016/01/groovy-goodness-customise-groovydoc.html"))
  (st/get-post-by-url conn "http://blog.mrhaki.com/2016/01/groovy-goodness-customise-groovydoc.html")

  (:content post-with-image)

  ;; empty line
  )
