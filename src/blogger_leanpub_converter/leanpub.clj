(ns blogger-leanpub-converter.leanpub
  (:require [blogger-leanpub-converter.image :as img]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.logging :as log]))

(defn- save-image!
  "Save image from given URL to given file."
  [uri output-file]
  (io/make-parents output-file)
  (with-open [in (io/input-stream uri)
              out (io/output-stream output-file)]
    (io/copy in out))
  output-file)

(defn download-links!
  "Download image links in given post and save to given output directory."
  [output-dir post]
  (let [{:keys [image-links]} post]
    (doseq [link image-links]
      (let [url (:download-url link)
            output-file (io/file output-dir (:image-ref link))]
        (img/convert-image! (save-image! url output-file))))))

(defn- chapter-file-name
  "Returns a file name for given chapter title."
  [chapter]
  (-> chapter
      str/lower-case
      (str/replace #"," "")
      (str/replace #"\s+" "-")
      (str ".txt")))

(defn save-chapter!
  [dir title content]
  (let [file-name (chapter-file-name title)
        file (io/file dir file-name)]
    (log/infof "Saving chapter %s" title)
    (io/make-parents file)
    (spit file content)))

(defn- get-chapter-file-names
  "Returns a list of chapter file names from given chapters."
  [chapters]
  (map (fn [c] (-> c :title chapter-file-name)) chapters))

(defn- save-chapters!
  "Save chapters to files."
  [output-dir chapters]
  (doseq [chapter chapters]
    (save-chapter! output-dir (:title chapter) (:markdown chapter))
    (doseq [post (:posts chapter)]
      (download-links! (io/file output-dir "images") post))))

(defn save-sample!
  "Save sample chapters to files."
  [dir manuscript]
  (let [file-name "Sample.txt"
        output-file (io/file dir file-name)
        chapters (filter #(:sample %) (:chapters manuscript))
        chapter-file-names (get-chapter-file-names chapters)]
    (log/infof "Saving %s" file-name)
    (io/make-parents output-file)
    (spit output-file (str/join \newline chapter-file-names))
    (save-chapters! dir chapters)))

(defn- save-frontmatter!
  "Save frontmatter file from given sections."
  [output-dir sections]
  (doseq [section sections]
    (save-chapter! output-dir (:title section) (:content section))))

(defn- save-instructions!
  "Save instructions file from given content."
  [output-dir content]
  (save-chapter! output-dir content (format "{%s}" content)))

(defn save-book!
  "Save Leanpub book from given manuscript."
  [dir manuscript]
  (let [file-name "Book.txt"
        output-file (io/file dir file-name)
        front-matter (:front-matter manuscript)
        chapters (:chapters manuscript)
        chapter-file-names (get-chapter-file-names chapters)]

    (log/infof "Saving %s" file-name)
    (io/make-parents output-file)

    (spit output-file "frontmatter.txt\n")
    (save-instructions! dir "frontmatter")

    (spit output-file "about-me.txt\n" :append true)
    (spit output-file "introduction.txt\n" :append true)
    (save-frontmatter! dir front-matter)

    (spit output-file "mainmatter.txt\n" :append true)
    (save-instructions! dir "mainmatter")

    (spit output-file (str/join \newline chapter-file-names) :append true)
    (save-chapters! dir chapters)))


(comment

  (chapter-file-name "Syntax, Groovy")
  (save-chapter! "sample" "Syntax" "sample content1")

  (assoc {:posts ["a"]} :html-posts ["b"])
  
  (convert-manuscript notebook)

  (filter #(:sample %) [{:title "h1" :sample true :markdown "# Syntax"} {:title "h2" :markdown "# Async"}])
  (save-sample! "sample" (filter #(:sample %) [{:title "Syntax" :sample true :markdown "# Syntax"} {:title "h2"}]))
  (map (fn [c] (-> c :title chapter-file-name)) [{:title "h1" :markdown "# Syntax"} {:title "h2"}])
  
  (def notebook {:title "Notebook",
                 :chapters [{:title "h1",
                             :posts [{:id "7132767448528120171",
                                      :url "http://blog.mrhaki.com/2020/07/clojure-goodness-replacing-characters.html",
                                      :labels #{"Clojure:Goodness"
                                                "ClojureGoodness:Strings"
                                                "Clojure 1.10.1"
                                                "Clojure"},
                                      :content "<p>The <code>clojure.string</code> namespace contains a lot of useful functions to work with string values. The <code>escape</code> function can be used to replace characters in a string with another character. The function accepts as first argument the string value and the second argument is a map. The map has characters as key that need to be replaced followed by the value it is replaced with. For example the map <code>{\\a 1 \\b 2}</code> replaces the character <code>a</code> with <code>1</code> and the character <code>b</code> with <code>2</code>.</p><p>In the following example code we use the <code>escape</code> function in several cases:</p>",
                                      :updated "2020-07-08T15:26:18+02:00",
                                      :etag "\"dGltZXN0YW1wOiAxNTk0MjE0Nzc4NjM0Cm9mZnNldDogNzIwMDAwMAo\"",
                                      :title "Clojure Goodness: Replacing Characters In A String With escape Function",
                                      :published "2020-07-08T15:26:00+02:00"}]}
                            {:title "h2",
                             :posts [{:id "7356010244958013577",
                                      :url "http://blog.mrhaki.com/2021/03/gradle-goodness-enabling-preview.html",
                                      :labels #{"Gradle 6.8.3"
                                                "Gradle:Goodness"
                                                "GradleGoodness:Kotlin"
                                                "GradleGoodness:Java"
                                                "Gradle"},
                                      :content "<p>Java introduced preview features in the language since Java 12.
This features can be tried out by developers, but are still subject to change and can even be removed in a next release.
By default the preview features are not enabled when we want to compile and run our Java code.
We must explicitly specify that we want to use the preview feature to the Java compiler and Java runtime using the command-line argument <code>--enable-preview</code>.
In Gradle we can customize our build file to enable preview features.
We must customize tasks of type <code>JavaCompile</code> and pass <code>--enable-preview</code> to the compiler arguments.
Also tasks of type <code>Test</code> and <code>JavaExec</code> must be customized where we need to add the JVM argument <code>--enable-preview</code>.</p>",
:updated "2021-03-05T22:48:55+01:00",
                      :etag "\"dGltZXN0YW1wOiAxNjE0OTgwOTM1MzI4Cm9mZnNldDogMzYwMDAwMAo\"",
                      :title "Gradle Goodness: Enabling Preview Features For Java",
                      :published "2021-03-05T22:44:00+01:00"}]}]})
  
  ,)
