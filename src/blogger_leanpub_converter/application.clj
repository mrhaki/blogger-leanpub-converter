(ns blogger-leanpub-converter.application
  (:require [com.stuartsierra.component :as component]
            [blogger-leanpub-converter.storage :as storage]
            [clojure.tools.logging :as log]))

(defrecord Database [dir connection]
  component/Lifecycle
  (start
    [component]
    (log/infof "Starting database %s" (:dir component))
    (if connection
      component
      (let [schema {:id     {:db/valueType :db.type/string
                             :db/unique    :db.unique/identity}
                    :url    {:db/valueType :db.type/string
                             :db/unique    :db.unique/identity}
                    :labels {:db/valueType   :db.type/string
                             :db/cardinality :db.cardinality/many}}]
        (assoc component :connection (storage/get-connection dir schema)))))

  (stop
    [component]
    (log/info "Stopping database")
    (assoc component :connection (storage/close-connection connection))))

(defn new-database
  [dir]
  (map->Database {:dir dir}))

(defn production-system
  "Configure production system with database"
  [config]
  (let [{:keys [database-dir]} config]
    (component/system-map
     :database (new-database database-dir))))

(defn test-system
  []
  (component/system-map
   :database (new-database "./build/test/blogger-db")))

(defn example-system
  [config]
  (let [{:keys [data-dir]} config]
    (component/system-map
     :database (new-database data-dir))))

(defn start
  "Performs side effects to initialize the system, acquire resources,
  and start it running. Returns an updated instance of the system."
  [system]
  (component/start system))

(defn stop
  "Performs side effects to shut down the system and release its
  resources. Returns an updated instance of the system."
  [system]
  (component/stop system))

(comment
  (test-system)

  (def system (example-system {:data-dir "/Users/mrhaki/Documents/blogger-db"}))

  (:database (component/start system))
  (:database system)

  (component/stop system))
