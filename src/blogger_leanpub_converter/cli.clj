(ns blogger-leanpub-converter.cli
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.string :as str]))

(def valid-books #{"awesome-asciidoctor"
                   "clojure-goodness"
                   "dataweave-delight"
                   "gradle-goodness"
                   "groovy-goodness"
                   "spocklight"})

(def cli-options
  [["-b" "--book BOOK" (str "The book to create (" (str/join ", " (sort valid-books)) ")")]
   ["-h" "--help"]])

(defn- usage
  [summary]
  (->> ["Options" summary]
       (str/join \newline)))

(defn- error-msg
  [errors]
  (str "The following errors occurred while parsing the command" 
       \newline 
       (str/join \newline errors)))

(defn validate-args
  "Validate arguments and return a map indicating the program's exit status."
  [args]
  (let [{:keys [options _ errors summary]} (parse-opts args cli-options)]
    (cond
      (:help options) {:exit-message (usage summary) :ok? true}
      errors {:exit-message (error-msg errors) :ok? false}
      (valid-books (:book options)) {:book (:book options)}
      :else {:exit-message (usage summary) :ok? false})))

(defn exit
  [status msg]
  (println msg)
  (System/exit status))

(comment
  (def opts (parse-opts ["-h" "test-book1"] cli-options))
  opts
  (usage (:summary opts))
  (error-msg (:errors opts))

  (validate-args ["-h"])
  (validate-args ["--book" "groovy-goodness"])

  (sort valid-books)

  cli-options
  )