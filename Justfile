# Show all tasks
default:
	@just --list

# Create Leanpub markdown for Awesome Asciidoctor notebook
asciidoctor: (main "asciidoctor")

# Create Leanpub markdown for Dataweave Delight notebook
dataweave: (main "dataweave")

# Create Leanpub markdown for Clojure Goodness notebook
clojure: (main "clojure")

# Create Leanpub markdown for Gradle Goodness notebook
gradle: (main "gradle")

# Create Leanpub markdown for Groovy Goodness notebook
groovy: (main "groovy")

# Create Leanpub markdown for Spocklight notebook
spock: (main "spock")

# Create Leanpub markdown file for given notebook
main book:
    clj -M:main:{{book}}

# Run tests and watch
test-watch: 
    clj -X:test:watch

# Run all tests
test:
    clj -X:test

# Create JAR file that can be run with java -jar
uberjar:
    clj -T:build uberjar

# Remove build directory
clean:
    clj -T:build clean