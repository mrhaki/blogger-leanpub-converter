(ns user
  (:require [blogger-leanpub-converter.application :as app]
            [blogger-leanpub-converter.config :as config]
            [clojure.tools.namespace.repl :refer (refresh)]
            [clojure.pprint :as pp]
            [clojure.test :refer [deftest testing is are]]
            [clojure.repl :refer :all]))

(def system nil)

(defn init
  "Constructs the current development system."
  []
  (alter-var-root #'system
                  (constantly (app/example-system {}))))

(defn start
  "Starts the current development system."
  []
  (alter-var-root #'system app/start))

(defn stop
  "Shuts down and destroys the current development system."
  []
  (alter-var-root #'system
                  (fn [s] (when s (app/stop s)))))

(defn go
  "Initializes the current development system and starts it running."
  []
  (init)
  (start))

(defn reset []
  (stop)
  (refresh :after 'user/go))

(comment

  (reset)

  (config/get-config "test-book")

  (or (and (set? #{:href :src})
           (#{:href :src} :p))
      (and (= :p :p)))


  (pp/pprint {:title "Hello"}) 
  (add-tap (bound-fn* pp/pprint))
  (tap> {:title "Hello"})
  (tap> "Hello")
  (remove-tap pp/pprint)

  (defn full-name
    [user]
    (str (:first-name user) " " (:last-name user)))
  
  (full-name {:first-name "Hubert" :last-name "Klein Ikkink"})

  (deftest test-full-name
    (are [user result] (= (full-name user) result)
      {:first-name "Hubert" :last-name "Klein Ikkink"} "Hubert Klein Ikkink"))

  (deftest math
    (testing "get maximum of 2 numbers"
      (are [a b c] (is (= c (Math/max a b)))
        5  1   5
        10 20  20
        100 20 100
        )))
  
  (doc clojure.test/thrown?)

  (source clojure.test)

  
  ;; empty line
  )