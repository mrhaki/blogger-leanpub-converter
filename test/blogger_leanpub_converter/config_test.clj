(ns blogger-leanpub-converter.config-test
  (:require [clojure.test :refer [deftest testing is]]
            [blogger-leanpub-converter.config :as config]))

(def config-file "./test/.blogger.edn")

(deftest test-get-config
  (testing "get-config returns a map"
    (is (map? (config/get-config "test-book" :config-file config-file))))

  (testing "get-config returns expected keys"
    (let [config (config/get-config "test-book" :config-file config-file)]
      (is (contains? config :database-dir))
      (is (contains? config :manuscript))
      (is (contains? config :output-dir))))

  (testing "get-config returns expected values"
    (let [config (config/get-config "test-book" :config-file config-file)]
      (is (= "test" (:database-dir config)))
      (is (= "test-book-notebook.edn" (:manuscript config)))
      (is (= "leanpub/test-book-notebook" (:output-dir config))))))
