(ns blogger-leanpub-converter.pandoc-test
  (:require [clojure.test :refer [deftest testing is]]
            [blogger-leanpub-converter.pandoc :as sut]))

(deftest ^:pandoc html->markdown_leanpub
  (testing "convert HTML with code block to Markdown"
    (is (= (sut/html->markdown_leanpub "<html><head></head><body><pre class=\"java\">import test;</pre></body></html>") "~~~ java\nimport test;\n~~~\n")))
  
  (testing "convert HTML with table to Markdown"
    (is (= (sut/html->markdown_leanpub "<html><head></head><body><p><table><tr><th>Col1</th><th>Col2</th></tr><tr><td>1</td><td>2</td></tr></table></p></body></html>") "| Col1 | Col2 |\n|------|------|\n| 1    | 2    |\n")))
  
  (testing "convert HTML to Markdown"
    (is (= (sut/html->markdown_leanpub "<html><head><body><h1 id=\"h1\">Title</h1><p><a href=\"https://example.com\">Link</a></p><p><b>bold</b><i>italic</i><code>code</code></body></html>") "# Title {#h1}\n\n[Link](https://example.com)\n\n**bold***italic*`code`\n"))))


(comment
  (sut/html->markdown_leanpub "<html><head></head><body><p><table><tr><th>Col1</th><th>Col2</th></tr><tr><td>1</td><td>2</td></tr></table></p></body></html>") 

  (sut/html->markdown_leanpub "<html><head><body><h1 id=\"h1\">Title</h1><p><a href=\"https://example.com\">Link</a></p><p><b>bold</b><i>italic</i><code>code</code></body></html>")

  ;; Empty line
  )