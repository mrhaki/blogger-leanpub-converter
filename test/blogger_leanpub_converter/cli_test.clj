(ns blogger-leanpub-converter.cli-test
  (:require [clojure.test :refer [deftest testing is]]
            [blogger-leanpub-converter.cli :as cli]))

(deftest test-validate-args
  (testing (let [result (cli/validate-args ["-h"])]
             (is (contains? result :exit-message))
             (is (contains? result :ok?))
             (is (:ok? result))
             (is (= "Options
  -b, --book BOOK  The book to create (awesome-asciidoctor, clojure-goodness, dataweave-delight, gradle-goodness, groovy-goodness, spocklight)
  -h, --help" (:exit-message result))))
    "help argument should return exit-message with usage and ok exit status"

    (let [result (cli/validate-args ["--help"])]
      (is (contains? result :exit-message))
      (is (contains? result :ok?))
      (is (:ok? result))
      (is (= "Options
  -b, --book BOOK  The book to create (awesome-asciidoctor, clojure-goodness, dataweave-delight, gradle-goodness, groovy-goodness, spocklight)
  -h, --help" (:exit-message result)))))

  (testing "book argument should return map with book"
    (let [result (cli/validate-args ["-b" "awesome-asciidoctor"])]
      (is (contains? result :book))
      (is (= "awesome-asciidoctor" (:book result))))

    (let [result (cli/validate-args ["--book" "awesome-asciidoctor"])]
      (is (contains? result :book))
      (is (= "awesome-asciidoctor" (:book result)))))

  (testing "invalid book argument should return exit-message with error and not ok exit status"
    (let [result (cli/validate-args ["-b" "test-book"])]
      (is (contains? result :exit-message))
      (is (contains? result :ok?))
      (is (false? (:ok? result)))
      (is (= "Options
  -b, --book BOOK  The book to create (awesome-asciidoctor, clojure-goodness, dataweave-delight, gradle-goodness, groovy-goodness, spocklight)
  -h, --help" (:exit-message result)))))

  (testing "invalid argument should return exit-message with error and not ok exit status"
    (let [result (cli/validate-args ["--invalid"])]
      (is (contains? result :exit-message))
      (is (contains? result :ok?))
      (is (false? (:ok? result)))
      (is (= "The following errors occurred while parsing the command
Unknown option: \"--invalid\"" (:exit-message result))))))

(comment
  
  ,)