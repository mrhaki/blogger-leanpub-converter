(ns blogger-leanpub-converter.markdown-test
  (:require [blogger-leanpub-converter.markdown :as markdown]
            [clojure.test :refer [deftest is are testing]]))

(deftest fix-code-block
  (testing "add {lang=text} for code blocks"
    (is (= "{lang=text}
~~~
$ mkdir spock-intro
~~~
" (markdown/fix-code-block "~~~ plain
$ mkdir spock-intro
~~~
"))))
  
  (testing "keep markdown for code blocks without type"
    (is (= "~~~\n$ mkdir spock-intro\n~~~\n" (markdown/fix-code-block "~~~\n$ mkdir spock-intro\n~~~\n")))))

(deftest fix-section-title
  (testing "add link to section title"
    (are [section-id url] (= (str section-id \newline "## Section title") (markdown/fix-section-title "## Section title" url))
      "{#id-2016-03-blog}" "https://blog.mrhaki.com/2016/03/blog.html"
      "{#id-2016-03-blog}" "http://blog.mrhaki.com/2016/03/blog.html")))