(ns blogger-leanpub-converter.manuscript-test
  (:require [blogger-leanpub-converter.manuscript :as manuscript]
            [clojure.test :refer [deftest testing is]]))

(deftest read-manuscript
  (testing "read manuscript file in EDN format"
    (let [manuscript (manuscript/read-manuscript "test-resources" "book.edn")]
      (is (= "Notebook" (:title manuscript)))
      (is (= "About Me" (-> manuscript :front-matter first :title)))
      (is (= "# About Me" (-> manuscript :front-matter first :content)))
      (is (= "Introduction" (-> manuscript :front-matter second :title)))
      (is (= "# Introduction" (-> manuscript :front-matter second :content)))
      (is (= "Chapter1" (-> manuscript :chapters first :title)))
      (is (= ["link1" "link2"] (-> manuscript :chapters first :posts))))))

(comment
  ;; empty line
  )