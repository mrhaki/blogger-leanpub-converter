(ns blogger-leanpub-converter.storage-test
  (:require [clojure.test :refer [deftest testing is]]
            [datalevin.core :as d]
            [blogger-leanpub-converter.storage :as storage]
            [blogger-leanpub-converter.application :as app]))

(defn- upsert-blog
  [conn]
  (let [post {:id        "7356010244958013577"
              :published "2021-03-05T22:44:00+01:00"
              :updated   "2021-03-05T22:48:55+01:00"
              :url       "http://blog.mrhaki.com/2021/03/gradle-goodness.html"
              :title     "Post Title"
              :content   "<p>Test Blog Post</p>"
              :labels    ["Gradle"]
              :etag      "\"dGltZXN0YW1\""}]
    (d/transact! conn [post])))

(deftest get-blog-by-url-test
  (testing "Return post by url"
    (let [test-system (app/start (app/test-system))
          conn (->> test-system :database :connection)]
      (upsert-blog conn)
      (is (= {:id        "7356010244958013577"
              :url       "http://blog.mrhaki.com/2021/03/gradle-goodness.html"
              :labels    #{"Gradle"}
              :content   "<p>Test Blog Post</p>"
              :updated   "2021-03-05T22:48:55+01:00"
              :etag      "\"dGltZXN0YW1\""
              :title     "Post Title"
              :published "2021-03-05T22:44:00+01:00"} (storage/get-post-by-url conn "http://blog.mrhaki.com/2021/03/gradle-goodness.html")))
      (app/stop test-system))))