(ns blogger-leanpub-converter.image-test
  (:require [blogger-leanpub-converter.image :as image]
            [clojure.test :refer [deftest testing is]]
            [clojure.java.io :as io])
  (:import [java.nio.file Files]))

(defn- files-equal?
  "Check if files are equal."
  [file1 file2]
  (let [path1 (.toPath file1)
        path2 (.toPath file2)
        mismatch (Files/mismatch path1 path2)]
    (= -1 mismatch)))

(defn- image-file
  "Return a File in the test resources directory."
  [filename]
  (let [base-dir "test-resources"]
    (io/file base-dir filename)))

(deftest ^:sips convert-image!
  (testing "convert image that is in the width and height boundaries"
    (is (files-equal? (image/convert-image! (image-file "image-boundaries-source.png")) (image-file "image-boundaries-expected.png"))))

  (testing "convert image that is too high"
    (let [source-filename "image-height-source.png"
          target-filename "image-height-work.png"]
      (io/copy (image-file source-filename) (image-file target-filename))
      (is (files-equal? (image/convert-image! (image-file target-filename)) (image-file "image-height-expected.png")))))

  (testing "convert image that is too wide"
    (let [source-filename "image-width-source.png"
          target-filename "image-width-work.png"]
      (io/copy (image-file source-filename) (image-file target-filename))
      (is (files-equal? (image/convert-image! (image-file target-filename)) (image-file "image-width-expected.png"))))))

(comment

  (is (files-equal? (image-file "image-boundaries-source.png") (image-file "image-boundaries-expected.png")))

  ;; empty line
  )