(ns blogger-leanpub-converter.html-test
  (:require [blogger-leanpub-converter.html :as html]
            [clojure.test :refer [deftest testing is]]))

(deftest make-url-id
  (testing "url should be transformed to id"
    (is (= "id-2021-03-post-title" (html/make-url-id "http://blog.mrhaki.com/2021/03/post-title.html")))
    (is (= "id-2021-03-post-title" (html/make-url-id "https://blog.mrhaki.com/2021/03/post-title.html")))))

(deftest html->hiccup
  (testing "transform HTML to hiccup format"
    (is (= '([:html {} [:head {}] [:body {} [:p {} "Test Blog Post"]]]) (html/html->hiccup "<p>Test Blog Post</p>")))))

(deftest fix-html
  (let [title "blog-title"
        url "url"
        date "2021-03-05T22:44:00+01:00"]

    (testing "replace-http-with-https"
      (testing "replace http with https for href and src attributes"
        (is (.contains (html/fix-html "<a href=\"http://server\">link</a>" title url date) "<a href=\"https://server\">link</a>"))
        (is (.contains (html/fix-html "<img src=\"http://server\"/>" title url date) "<img src=\"https://server\" />")))

      (testing "do not replace https for href and src attributes if it is already used"
        (is (.contains (html/fix-html "<a href=\"https://server\">link</a>" title url date) "<a href=\"https://server\">link</a>"))
        (is (.contains (html/fix-html "<img src=\"https://server\"/>" title url date) "<img src=\"https://server\" />"))))

    (testing "replace brush with brush values"
      (is (.contains (html/fix-html "<pre class=\"brush:java\">code</pre>" title url date) "<pre class=\"java\">code</pre>"))
      (is (.contains (html/fix-html "<pre class=\"brush:plain;light:true\">code</pre>" title url date) "<pre class=\"plain\">code</pre>")))

    (testing "replace div with p"
      (is (.contains (html/fix-html "<div>Test</div>" title url date) "<p>Test</p>")))

    (testing "replace h2 with h3"
      (is (.contains (html/fix-html "<h2>Test</h2>" title url date) "<h3>Test</h3>")))

    (testing "add title to html"
      (is (.contains (html/fix-html "" "Gradle Goodness: Testing a New Build" url date) "<h2>Testing a New Build</h2>")))

    (testing "replace-blogger-base-url"
      (testing "replace blogspot base url with blog.mrhaki.com"
        (is (.contains (html/fix-html "<a href=\"https://mrhaki.blogspot.nl/2021/03/post-title.html\">link</a>" title url date) "<a href=\"https://blog.mrhaki.com/2021/03/post-title.html\">link</a>")))

      (testing "do not replace other base url with blog.mrhaki.com"
        (is (.contains (html/fix-html "<a href=\"https://www.google.nl/2021/03/post-title.html\">link</a>" title url date) "<a href=\"https://www.google.nl/2021/03/post-title.html\">link</a>"))))

    (testing "internal-link-blog-url"
      (testing "replace blog links to # references for same post"
        (is (.contains (html/fix-html "<a href=\"https://blog.mrhaki.com/2021/03/post-title.html\">link</a>" title "https://blog.mrhaki.com/2021/03/post-title.html" date) "<a href=\"#id-2021-03-post-title\">link</a>")))

      (testing "do not replace blog links to # references for other post"
        (is (.contains (html/fix-html "<a href=\"https://blog.mrhaki.com/2021/03/post-title.html\">link</a>" title "https://blog.mrhaki.com/2021/03/other-post-title.html" date) "<a href=\"https://blog.mrhaki.com/2021/03/post-title.html\">link</a>"))))

    (testing "add-post-reference"
      (testing "add link to original post"
        (is (.contains (html/fix-html "" title "https://blog.mrhaki.com/2021/03/post-title.html" date) "<p><a href=\"https://blog.mrhaki.com/2021/03/post-title.html\">Original post</a> written on March 5, 2021</p></body>")))

      (testing "add link to original post with https"
        (is (.contains (html/fix-html "" title "http://blog.mrhaki.com/2021/03/post-title.html" date) "<p><a href=\"https://blog.mrhaki.com/2021/03/post-title.html\">Original post</a> written on March 5, 2021</p></body>"))))

    (testing "replace image with reference"
      (is (.contains (html/fix-html "<a href=\"http://1.bp.blogspot.com/-9qsqVoEDhMM/VflLErnPxeI/AAAAAAAALTc/s5NjfIfD-5M/s1600/idea-intention.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" src=\"http://1.bp.blogspot.com/-9qsqVoEDhMM/VflLErnPxeI/AAAAAAAALTc/s5NjfIfD-5M/s640/idea-intention.png\" /></a>" title url date) "<img src=\"images/idea-intention.png\" />")))))

(deftest get-image-link
  (testing "get image links"
    (is (.contains (html/get-image-links "<div class=\"separator\" style=\"clear: both; text-align: center;\"><a href=\"http://4.bp.blogspot.com/-R7NV9p4epZA/VflKi2NVP3I/AAAAAAAALTU/x2IyUiES6-g/s1600/idea-intention-convert-string.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" src=\"http://4.bp.blogspot.com/-R7NV9p4epZA/VflKi2NVP3I/AAAAAAAALTU/x2IyUiES6-g/s640/idea-intention-convert-string.png\" /></a></div>") {:download-url "http://4.bp.blogspot.com/-R7NV9p4epZA/VflKi2NVP3I/AAAAAAAALTU/x2IyUiES6-g/s1600/idea-intention-convert-string.png" :image-ref "idea-intention-convert-string.png"}))))

(comment
  (.contains "abcd" "abc")

  (html/fix-html "<div class=\"separator\" style=\"clear: both; text-align: center;\"><a href=\"http://1.bp.blogspot.com/-9qsqVoEDhMM/VflLErnPxeI/AAAAAAAALTc/s5NjfIfD-5M/s1600/idea-intention.png\" imageanchor=\"1\" style=\"margin-left: 1em; margin-right: 1em;\"><img border=\"0\" src=\"http://1.bp.blogspot.com/-9qsqVoEDhMM/VflLErnPxeI/AAAAAAAALTc/s5NjfIfD-5M/s640/idea-intention.png\" /></a></div>" "title" "url" "2021-03-05T22:44:00+01:00")

  (html/fix-html "" "title" "https://blog.mrhaki.com/2021/03/other-post-title.html" "2021-03-05T22:44:00+01:00")

  (html/fix-html "<a href=\"https://blog.mrhaki.com/2021/03/post-title.html\">link</a>" "title" "https://blog.mrhaki.com/2021/03/post-title.html" "2021-03-05T22:44:00+01:00")

  (html/fix-html "<a href=\"https://mrhaki.blogspot.nl/2021/03/post-title.html\">link</a>" "title" "url" "2021-03-05T22:44:00+01:00")

  (html/fix-html "<pre class=\"brush:java;light:true\">code</pre>" "Gradle Goodness:Testing a New Build" "url" "2021-03-05T22:44:00+01:00")

  (html/fix-html "<img src=\"http://server\"/>" "title" "url" "2021-03-05T22:44:00+01:00")

  ,)